# cpanel-website-preview

'Website Preview' option as an alternative to the built-in 'IP Address' preview.

To install, run the following...

curl -s https://gitlab.com/brixly/cpanel-website-preview/raw/master/install.sh | bash