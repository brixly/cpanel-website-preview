#!/bin/bash

APIKEY=$1
cd /root
rm -Rf /root/cpanel-website-preview/
git clone https://gitlab.com/brixly/cpanel-website-preview.git
/scripts/install_plugin cpanel-website-preview/wp_preview --theme=paper_lantern
/scripts/install_plugin cpanel-website-preview/wp_preview --theme=jupiter
cp cpanel-website-preview/wp_preview/wp_preview.live.php /usr/local/cpanel/base/frontend/paper_lantern/.
cp cpanel-website-preview/wp_preview/wp_preview.live.php /usr/local/cpanel/base/frontend/jupiter/.
cp cpanel-website-preview/wp_preview/skipdns.live.php /usr/local/cpanel/base/frontend/paper_lantern/.
cp cpanel-website-preview/wp_preview/skipdns.live.php /usr/local/cpanel/base/frontend/jupiter/.
echo $APIKEY > /etc/skipdns.key
