<?php
        //phpinfo();
  error_reporting(E_ALL);
  //phpinfo();
  ini_set('display_errors', 1);
  include("/usr/local/cpanel/php/cpanel.php");  // Instantiate the CPANEL object.
  $cpanel = new CPANEL();                       // Connect to cPanel - only do this once.

  $user_info = $cpanel->uapi(
      'Variables',
      'get_user_information'
  );
  


  $homedir = $user_info['cpanelresult']['result']['data']['home'];
  $skipdns_dir = $homedir . "/etc/skipdns";

        if(isset($_GET['ajax']) || isset($_POST['ajax'])) {

                if($_POST['ajax'] == 'generateURL') {
      $payload = array();
      $payload['url'] = urlencode($_POST["url"]);
      $payload['ip_address'] = urlencode($_POST["ip"]);
      $link = doAPI('links', 'POST', $payload);
      // file_put_contents($cpanel)
      $domain_file = $skipdns_dir . "/" . $_POST['url'];
      
      if (!file_exists($homedir . "/etc/skipdns")) {
          mkdir($homedir . "/etc/skipdns", 0755, true);
      };
      if(!empty($link)) file_put_contents($domain_file, $link);

      die();


                }
        }

  print $cpanel->header( "Website Preview" );      // Add the header.

  $api_key = file_get_contents('/etc/skipdns.key');
  if(empty($api_key)) {
    echo "<div class='alert alert-danger'>Error: The API Key is either inaccessible, or is missing!</div>";
  };

  function doAPI($path, $type = 'POST', $data){
    
    $api_key = trim(file_get_contents('/etc/skipdns.key'));

    $url = "https://www.skipdns.link/api/v1/links"; // Keep the original URL
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
       "Authorization: Bearer " . $api_key,
       "Content-Type: application/x-www-form-urlencoded"
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    
    $data = "url=" . $data["url"] . "&ip_address=" . $data["ip_address"];
    
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    // Use CURLOPT_RESOLVE to force DNS resolution to the specific IP address
    curl_setopt($curl, CURLOPT_RESOLVE, array("www.skipdns.link:443:138.197.49.74"));

    // For debugging purposes, consider re-enabling SSL verification after testing
    // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
    // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);

    $resp = json_decode(curl_exec($curl))->data->short_url;
    print_r($resp);
    curl_close($curl);
    return $resp;
}

  function isExpired($filename){
    
    if(!file_exists($filename)) return true;
    
    if (time()-filemtime($filename) > 14 * 24 * 3600) {
      // file older than 14 days
      return true;
    } else {
      // file younger than 14 days
      return false;
    }
    
  }



        // List the account's domains.
        $get_domain_list = $cpanel->uapi(
            'DomainInfo', 'list_domains'
        );
        $get_domain_list = $get_domain_list['cpanelresult']['result']['data'];

        // Get the accounts IP address...
        $user_info = $cpanel->uapi(
            'Variables', 'get_user_information'
        );
        $user_ip = $user_info['cpanelresult']['result']['data']['ip'];

?>
  <link href="bolt-cache/smart_wizard.css" rel="stylesheet" />
  <div class="top-notice">
        <h4>What is Website Preview?</h4>

        <p>Our website preview facility has been designed to give you a temporary link to your site, without having to update your DNS.</p>

        <p>This is ideal if you wish to check a site is functioning before updating the nameservers / DNS of your domain, or for providing your clients a preview of their website before 'go-live'.</p>

        <p>If you are having any issues with the preview URL, you can still generate a preview URL using the old method by clicking <a href="wp_preview.live.php">here</a>.</p>

        <p>Please note, that the links automatically expire after 14 days.</p>

  </div>

  <div class="centered">

    <div id="smartwizard">

      <div>
        <div id="step-1" class="">
                        <table class="table table-hover table-dark">
                          <thead>
                            <tr>
                              <th scope="col">Domain</th>
                              <th scope="col"></th>
                            </tr>
                          </thead>
                          <tbody>

                            <tr>
                                <th scope="row"><?php echo $get_domain_list['main_domain']; ?>  <span class="badge badge-secondary hide" style="font-size: 10px;">Proxy / Default</span></th>
                                <td>
                
                <?php
                $filename = $skipdns_dir . "/" . $get_domain_list['main_domain'];
                if(isExpired($filename)) { ?>
                  <button class="btn btn-info pull-right" onclick="callAPI('<?php echo $get_domain_list['main_domain'];?>', '<?php echo $user_ip;?>', $(this))"/>Generate Preview URL</button>
                <?php } else { 
                   $link = "https://" . file_get_contents($filename);
                ?>
                  <button class="btn btn-success pull-right" onclick="window.open('<?php echo $link ?>','_blank')"/><?php echo $link; ?></button>
                <?php } ?>
                
              </td>
                            </tr>


                            <?php foreach($get_domain_list['addon_domains'] as $domain) { ?>

                                    <tr>
                                        <th scope="row"><?php echo $domain; ?></th>
                                        <td>
                
                <?php
                $filename = $skipdns_dir . "/" . $domain;
                if(isExpired($filename)) { ?>
                  <button class="btn btn-info pull-right" onclick="callAPI('<?php echo $domain;?>', '<?php echo $user_ip;?>', $(this))"/>Generate Preview URL</button>
                <?php } else { 
                   $link = "https://" . file_get_contents($filename);
                ?>
                  <button class="btn btn-success pull-right" onclick="window.open('<?php echo $link ?>','_blank')"/><?php echo $link; ?></button>
                <?php } ?>
              
                </td>
                                    </tr>

                            <?php }; ?>


                            <?php foreach($get_domain_list['sub_domains'] as $domain) { ?>

                                    <tr>
                                        <th scope="row"><?php echo $domain; ?></th>
                                        <td>

                <?php
                $filename = $skipdns_dir . "/" . $domain;
                if(isExpired($filename)) { ?>
                  <button class="btn btn-info pull-right" onclick="callAPI('<?php echo $domain;?>', '<?php echo $user_ip;?>', $(this))"/>Generate Preview URL</button>
                <?php } else { 
                   $link = "https://" . file_get_contents($filename);
                ?>
                  <button class="btn btn-success pull-right" onclick="window.open('<?php echo $link ?>','_blank')"/><?php echo $link; ?></button>
                <?php } ?>

                </td>
                                    </tr>

                            <?php }; ?>

                          </tbody>
                        </table>

        </div>
        </div>
      </div>
    </div>





  </div>
<script>
function callAPI(domain, ip, obj){
  console.log(obj)
  $(obj).html("Generating URL ...");
  $(obj).addClass("disabled");
  $.ajax({
    type : "post",
    url : "skipdns.live.php",
    data : {
      ajax  : "generateURL",
      url : domain,
      ip : ip
    },
    success : function(data){
      console.log(data);
      // location.href = "https://" +data;
      // console.log(obj);
        $(obj).html("https://" + data);
        $(obj).removeClass("disabled");
        $(obj).removeClass("btn-info");
        $(obj).addClass("btn-success");
        $(obj).attr({
          "onclick" : "window.open('https://"+data+"', '_blank')"
        });
    }
  })
}
</script>

  <?php

    //var_dump("CONTAINS_MOD: $contains_mod");
    // AutoSSL - Force install of domain
    // Open URL in new window
    print $cpanel->footer();                      // Add the footer.
    $cpanel->end();                               // Disconnect from cPanel - only do this once.
?>


<style>

#content {
        margin-top: 0;
        padding-top: 35px;
        padding-bottom: 35px;
        min-height: 300px;
}

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 25%;

}

.top-notice {
        background: #12181d;
    padding: 15px;
    border-radius: 5px;
    font-size: 12px;
    color: white;
        margin-bottom: 35px;
        border: 2px solid #ff7101;
}

/* Style the buttons that are used to open the tab content */
.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

#current_template {
        font-weight: bold;
}

/* Create an active/current "tab button" class */
.tab button.active {
    background-color: #ccc;
}

#existing-config {
    margin: 10px 0;
    padding: 10px;
    background-color: #42f6b4;
    border-radius: 5px;
    border: 1px solid #24cc21;
    color: #1e7756;
}

/* Style the tab content */
.tabcontent {
        display: none;
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 75%;
    border-left: none;
}

.sw-btn-group {
  float: right !important;
}

.sw-btn-prev {
  float: left !important;
}

.tablinks img {
  max-width: 60%;
  margin: 0 auto;
}

.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-deck .card {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex: 1 0 0%;
    flex: 1 0 0%;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    margin-right: 15px;
    margin-bottom: 0;
    margin-left: 15px;
    padding-left: 0 !important;
    padding-right: 0 !important;
}

.box-shadow {
    box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .05);
}

.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.card-body {
        padding: 10px;
}

.card.col-sm-3 {
    width: 24%;
    margin: 4px !important;
}

.badge-success {
    color: #fff;
    background-color: #28a745;
}

.badge-warning {
    color: #212529;
    background-color: #ffc107;
}

.checked {
    color: orange;
}

.star-rating {
        padding: 10px;
}

.cpanel_body {
    max-width: none !important;
}

#devWarningBlock {
        display: none !important;
}

legend {
        padding: 20px !important;
}

.modal {
    overflow: initial;

}

.selected_domain {
        font-size: 18px;
    font-weight: bold;
    color: #b9b9b9;
}


.form-control-lg {
    height: calc(3.875rem + 10px);
    padding: 0.5rem 2rem;
    font-size: 1.75rem;
    line-height: 1.5;
    border-radius: .3rem;
}

.btn-toolbar {display: none;}

.container {width: 100%}

.sw-theme-default > ul.step-anchor > li.active > a {
    border: none !important;
    color: #fff !important;
    background: transparent !important;
    cursor: pointer;
        font-weight: bold;
}

.sw-theme-default > ul.step-anchor > li > a::after {
    content: "";
    background: #ff7101;
    height: 4px;
    position: absolute;
    width: 100%;
    left: 0px;
    bottom: 0px;
    -webkit-transition: all 250ms ease 0s;
    transition: all 250ms ease 0s;
    -webkit-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
}

.sw-theme-default > ul.step-anchor > li.done > a {
    border: none !important;
    color: #a9a9a9 !important;
    background: transparent !important;
    cursor: pointer;
}

.sw-theme-default .step-content {
    padding: 10px;
    border: 0px solid #D4D4D4;
    background-color: #FFF;
    text-align: left;
    border-radius: 0 0 5px 5px;
    display: block;
}

.step-anchor {
  background-color: #12181d;
}

</style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script>
        $ = jQuery;
  </script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<script src="bolt-cache/bolt-cache.js"></script>
<script src="bolt-cache/jquery.smartWizard.js"></script>
