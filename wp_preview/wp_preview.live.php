<?php

    // Enable Error Reporting for Script

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    // Initialise cPanel Object

    include("/usr/local/cpanel/php/cpanel.php");  
    $cpanel = new CPANEL();  

    // Define the Header

    print $cpanel->header( "Website Preview" );

    // Get some VARS

    $username = $_ENV['USER'];
    $home_path = $_ENV['HOME'];
    $server_hostname = gethostname();
    $hostname_prefix = explode('.', $server_hostname);
    $hostname_prefix = $hostname_prefix[0];

    $addonCreated = true;

    // Check if this is a WordPress install

    $wp_config_path = "$home_path/public_html/wp-config.php";
    $wp_config_file = (file_exists($wp_config_path)) ? file_get_contents($wp_config_path):'';
    
    $is_wordpress = file_exists($wp_config_path);
    
    // Create Domain Alias for accountname.server.cloudns.io

    $preview_url = $username . '.' . $hostname_prefix . '.' . 'mysitepreview.co.uk';

    $temp_url = $cpanel->api2(
            'AddonDomain', 'addaddondomain',
                    array(
                            'dir'           => 'public_html',
                            'newdomain'     => $preview_url,
                            'subdomain'     => "preview-" . $username
                    )
    );

    if(!isset($temp_url['cpanelresult']['error'])) {
        $temp_url['cpanelresult']['error'] = '';
    }

    $create_error = $temp_url['cpanelresult']['error'];

    $already_exists = (strpos($create_error, "already exists")) ? true:false;

    if(strpos($create_error, "has been reached")) {

		?>
		<div id="devWarningBlock" class="alert alert-warning">
            <span class="glyphicon glyphicon-exclamation-sign"></span>
            <div id="devWarningMessage" class="alert-message">
            	<p><strong><?php echo $create_error; ?></strong></p>
            	<p>Our Website Preview feature in cPanel requires a spare 'addon' available in your hosting package. Please contact your hosting provider and request they increase the addons available to your account.</p>
            </div>
        </div>
		<?php 

    	die();

    }

    // Change wp-config.php to include 'accept all domains'

    if($is_wordpress) {

        // Check if wp-config.php already contains WP_SITEURL

        $contains_mod = (strpos($wp_config_file,'WP_SITEURL') > 0);

        if(!$contains_mod) {

	        $file_content = file_get_contents($wp_config_path);

			$content_before_string = strstr($file_content, "stop editing!", true);

			if (false !== $content_before_string) {
			    $line = count(explode(PHP_EOL, $content_before_string));
			    $line = $line - 1;
			}

			echo "We have added a small snippet of code to line $line for you, which allows your site to be previewed via the preview URL provided...";

			// Write the fix to the wp-config.php

			$f = fopen($wp_config_path, "r+");
			$wp_config_contents = file($wp_config_path);

			$inserted = array("\n /* CLOUDNS PREVIEW */ \n", "\$domain = sprintf('%s://%s', \$_SERVER['SERVER_PORT'] == 80 ? 'http' : 'https', \$_SERVER['SERVER_NAME']);", "\ndefine('WP_SITEURL', \$domain); define('WP_HOME', \$domain); \n \n");

			array_splice($wp_config_contents, $line, 0, $inserted);

			$wp_config_contents = implode("", $wp_config_contents);

			$modify = file_put_contents($wp_config_path, $wp_config_contents);

		}

	};

?>
	<style>

	.sw-theme-default .step-content,
	.sw-main .step-content {
		display: block !important;
	}

	.lds-ring {
	  display: inline-block;
	  position: relative;
	  width: 64px;
	  height: 64px;
	}
	.lds-ring div {
	  box-sizing: border-box;
	  display: block;
	  position: absolute;
	  width: 51px;
	  height: 51px;
	  margin: 6px;
	  border: 6px solid #fff;
	  border-radius: 50%;
	  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
	  border-color: #428bca transparent transparent transparent;
	}
	.lds-ring div:nth-child(1) {
	  animation-delay: -0.45s;
	}
	.lds-ring div:nth-child(2) {
	  animation-delay: -0.3s;
	}
	.lds-ring div:nth-child(3) {
	  animation-delay: -0.15s;
	}
	@keyframes lds-ring {
	  0% {
	    transform: rotate(0deg);
	  }
	  100% {
	    transform: rotate(360deg);
	  }
	}

	.lds-ripple {
	  display: inline-block;
	  position: relative;
	  width: 64px;
	  height: 64px;
	  margin: 30px;
	}
	.lds-ripple div {
	  position: absolute;
	  border: 4px solid #428bca;
	  opacity: 1;
	  border-radius: 50%;
	  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
	}
	.lds-ripple div:nth-child(2) {
	  animation-delay: -0.5s;
	}
	@keyframes lds-ripple {
	  0% {
	    top: 28px;
	    left: 28px;
	    width: 0;
	    height: 0;
	    opacity: 1;
	  }
	  100% {
	    top: -1px;
	    left: -1px;
	    width: 58px;
	    height: 58px;
	    opacity: 0;
	  }
	}
	.preview_url_link {
		font-size: 2.5em;
	    padding: 20px;
	    background: #efefef;
	    border-radius: 40px;
	    margin: 50px 0;
	}
	</style>
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script>
	jQuery(document).ready(function(){		
		jQuery('.lds-ripple').delay(2000).fadeOut(1000);
		jQuery('.generating_title').delay(2500).fadeOut(function(){
			jQuery(this).text("Your Preview URL is...").fadeIn();
		});
		jQuery('.preview_url_link').delay(3000).fadeIn(1000);
		jQuery('#previewWarningDialog').delay(3000).fadeIn(1000);
	});
	</script>
	<div class="container" style="text-align: center;">
		<h3 class="generating_title">Generating Preview URL...</h3>
		<!--<div class="lds-ring"><div></div><div></div><div></div><div></div></div>-->
		<div class="lds-ripple"><div></div><div></div></div>
		<a target="_blank" href="http://<?php echo $preview_url; ?>"><h4 class="preview_url_link" style="display: none; font-size: 2.5em;"><?php echo $preview_url; ?></h4></a>

		<div id="previewWarningDialog" style="display: none;" class="alert alert-warning">
            <span class="glyphicon glyphicon-exclamation-sign"></span>
            <div id="previewWarningDialogContainer" class="alert-message">
            <ul>
			  <li>If you see a 'Sorry' page, then you will need to clear your caches in your browser, or try the link in 'Incognito Mode'.</li>
			  <li>If your site requires SSL, then you can now use the LetsEncrypt plugin to install a certificate to the new Preview URL.</li>
			  <li>If your site is WordPress, then we have already modified your wp-config.php file to work through the Preview URL.</li>
			</ul>      
            </div>
        </div>

	</div>

<?php 

    //var_dump("CONTAINS_MOD: $contains_mod");
    // AutoSSL - Force install of domain
    // Open URL in new window
    print $cpanel->footer();                      // Add the footer.
    $cpanel->end();                               // Disconnect from cPanel - only do this once.
?>
